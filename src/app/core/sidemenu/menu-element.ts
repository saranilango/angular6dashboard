export const menus = [
    {
        'name': 'Home',
        'icon': 'home',
        'link': false,
        'open': false,
        'chip': { 'value': 1, 'color': 'accent' },
        // 'sub': [
        //     {
        //         'name': 'Dashboard',
        //         'link': '/auth/dashboard',
        //         'icon': 'dashboard',
        //         'chip': false,
        //         'open': true,
        //     }
        // ]
    },
    {
        'name': 'Master',
        'icon': 'dvr',
        'link': false,
        'open': false,
        'sub': [
            {
                'name': 'Employer',
                'link': false,
                'icon': 'person',
                'chip': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Create/Update',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Fake Employer',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    }
                ]
            },
            {
                'name': 'Institution',
                'link': false,
                'icon': 'business',
                'chip': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Create/Update',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Fake Instution',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    }
                ]
            },
            {

                'name': 'Instution Entry',
                'link': false,
                'icon': 'business',
                'chip': false,
                'open': false,

            },
            {
                'name': 'Professional Reference Entry',
                'link': false,
                'icon': 'assignment',
                'chip': false,
                'open': false,
            },
            {
                'name': 'Create User',
                'link': false,
                'icon': 'how_to_reg',
                'chip': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Create User',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false,
                    },
                    {
                        'name': 'Component Mapping',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false,
                    }
                ]
            },
            {
                'name': 'Alert Rules',
                'link': false,
                'icon': 'add_alert',
                'chip': false,
                'open': false,
            },
            {
                'name': 'Call Charges List',
                'link': false,
                'icon': 'call',
                'chip': false,
                'open': false,
            },
            {

                'name': 'Country List',
                'link': false,
                'icon': 'map',
                'chip': false,
                'open': false,
            },
            {
                'name': 'Status List',
                'link': false,
                'icon': 'list_alt',
                'chip': false,
                'open': false,
            },
            {
                'name': 'Vendor Master',
                'link': false,
                'icon': 'widgets',
                'chip': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Create Vendor',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Create Vendor Service',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Vendor User Credential',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    }
                ]
            },

            {
                'name': 'Client Creation',
                'link': false,
                'icon': 'how_to_reg',
                'chip': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Client Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Case Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Case User Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Bulk Case Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Site Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Client User Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Billing User Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Billing Rule Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Billing Cycle Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Cancellation Rule Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    },
                    {
                        'name': 'Aadhhar Standlone client Creation',
                        'link': false,
                        'icon': false,
                        'chip': false,
                        'open': false
                    }
                ]
            },
            {
                'name': 'Email Queue',
                'link': false,
                'icon': 'email',
                'chip': false,
                'open': false,
            },
            {
                'name': 'Screening Status / Bulk Update',
                'icon': 'ballot',
                'link': false,
                'chip': false,
                'open': false,
            },
            // {
            //     'name': 'Progress Bar',
            //     'link': 'material-widgets/progress-bar',
            //     'icon': 'trending_flat',
            //     'chip': false,
            //     'open': false,
            // },
            {
                'name': 'Sales Team Client Mapping',
                'icon': 'transfer_within_a_station',
                'link': false,
                'open': false,
            },
            {
                'name': 'Address Tagging Report',
                'icon': 'pin_drop',
                'link': false,
                'chip': false,
                'open': false,
            },
            {
                'name': 'Screening Questions',
                'icon': 'question_answer',
                'link': false,
                'open': false,
            },
        ]
    },
    // {
    //     'name'   : 'Forms',
    //     'icon'   : 'mode_edit',
    //     'open'   : false,
    //     'link'   : false,
    //     'sub'    :  [
    //                     {
    //                         'name': 'Template Driven',
    //                         'icon': 'mode_edit',
    //                         'open'   : false,
    //                         'link':'forms/template_forms'
    //                     },
    //                     {
    //                         'name': 'Reactive Forms',
    //                         'icon': 'text_fields',
    //                         'open'   : false,
    //                         'link':'forms/reactive_forms'
    //                     }
    //                 ]
    // },
    {
        'name': 'Screening',
        'icon': 'movie_filter',
        'link': false,
        'open': false,
        // 'chip': { 'value': 2, 'color': 'accent' },
        'sub': [
            {
                'name': 'Indian client File',
                'icon': 'folder_shared',
                'link': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Indian Component',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Abroad Component',
                        'icon': false,
                        'link': false,
                        'open': false,
                    }
                ]
            },
            {
                'name': 'A-check or Netforce File',
                'icon': 'folder_shared',
                'link': false,
                'open': false,
            },
            {
                'name': 'Indian Client BGV Form Upload',
                'icon': 'present_to_all',
                'link': false,
                'open': false,
            }
        ]

    },
    {
        'name': 'Transaction',
        'icon': 'list',
        'link': false,
        'open': false,
    }, {
        'name': 'Dashboard',
        'icon': 'dashboard',
        'open': false,
        'link': false,
    }, {
        'name': 'Reports',
        'icon': 'bug_report',
        'open': false,
        'link': false,
        'sub': [
            {
                'name': 'Closure Report',
                'icon': 'report',
                'link': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Client Closure Report',
                        'iocn': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'QC Report',
                        'iocn': false,
                        'link': false,
                        'open': false,
                    }
                ]
            },
            {
                'name': 'Pending Report',
                'icon': 'subject',
                'link': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Pending List Report',
                        'iocn': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Pending High Priority',
                        'iocn': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Indian Pending List',
                        'iocn': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Not Send To QC-Pending QC Approval',
                        'iocn': false,
                        'link': false,
                        'open': false,
                    }
                ]
            },
            {
                'name': 'Client Report',
                'icon': 'library_books',
                'link': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Final Intrim Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Individual Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Supplymentary Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Bulk Final Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    }
                ]
            },
            {
                'name': 'Tracker Report',
                'icon': 'art_track',
                'link': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Monthly Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Daily Tracker Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Address Tracker Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Vendor Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    },
                    {
                        'name': 'Efficiency Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    }
                ]
            },
            {
                'name': 'File Level Tracker',
                'icon': 'file_copy',
                'link': false,
                'open': false,
            },
            {
                'name': 'Client Commendated Report',
                'icon': 'error_outline',
                'link': false,
                'open': false,
            },
            {
                'name': 'JCR File Download',
                'icon': 'save_alt',
                'link': false,
                'open': false,
            },
            {
                'name': 'Invoice Report',
                'icon': 'verified_user',
                'link': false,
                'open': false,
                'sub': [
                    {
                        'name': 'Client Invoice Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                        'sub': [
                            {
                                'name': 'Billed Case List',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'UnBilled Case List',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'Choose Packages',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'Generate Invoice/Estimate Cost',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'View Invoice',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'Generate Invoice Client List',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'Register Manual Invoice',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'Batch Editor( Bill Generation Case List)',
                                'icon': false,
                                'link': false,
                                'open': false,
                            },
                            {
                                'name': 'Invoice Clip',
                                'icon': false,
                                'link': false,
                                'open': false,
                            }
                        ]
                    },
                    {
                        'name': 'Vendor Invoice Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                        'sub': [
                            {
                                'name': 'Case List',
                                'icon': false,
                                'link': false,
                                'open': false,
                            }
                        ]
                    },
                    {
                        'name': 'A-Check & Net Force Invoice Report',
                        'icon': false,
                        'link': false,
                        'open': false,
                    }
                ]
            },
            {
                'name': 'Report Tracker',
                'icon': 'card_membership',
                'link': false,
                'open': false,
            },
            {
                'name': 'Insufficiency',
                'icon': 'waves',
                'link': false,
                'open': false,
            },
            {
                'name': 'Assigning Screen Owner',
                'icon': 'contact_mail',
                'link': false,
                'open': false,
            },
            {
                'name': 'Aadhaar Verification',
                'icon': 'thumb_up',
                'link': false,
                'open': false,
            }
        ]
    }
    // , {
    //     'name': 'Pages',
    //     'icon': 'content_copy',
    //     'open': false,
    //     'link': false,
    //     'sub': [
    //         {
    //             'name': 'Login',
    //             'icon': 'work',
    //             'open': false,
    //             'link': '../login',
    //         }, {
    //             'name': 'Services',
    //             'icon': 'local_laundry_service',
    //             'open': false,
    //             'link': 'pages/services',
    //         }, {
    //             'name': 'Contact',
    //             'icon': 'directions',
    //             'open': false,
    //             'link': 'pages/contact'
    //         }
    //     ]
    // }
    // , {

    //     'name': 'Charts',
    //     'icon': 'pie_chart_outlined',
    //     'open': false,
    //     'link': false,
    //     'sub': [
    //         {
    //             'name': 'chartjs',
    //             'icon': 'view_list',
    //             'link': 'charts/chartjs',
    //             'open': false,

    //         },
    //         {
    //             'name': 'ngx-chart',
    //             'icon': 'show_chart',
    //             'open': false,
    //             'link': 'charts/ngx-charts',
    //         },
    //         {
    //             'name': 'nvd3',
    //             'icon': 'pie_chart',
    //             'open': false,
    //             'link': 'charts/nvd3-charts',
    //         }
    //     ]
    // }, {
    //     'name': 'maps',
    //     'icon': 'map',
    //     'open': false,
    //     'link': false,
    //     'sub': [
    //         {
    //             'name': 'google-map',
    //             'icon': 'directions',
    //             'link': 'maps/googlemap',
    //             'open': false,
    //         },
    //         {
    //             'name': 'leaflet-map',
    //             'icon': 'directions',
    //             'link': 'maps/leafletmap',
    //             'open': false,
    //         }
    //     ]
    // }
];
